from django.shortcuts import render, HttpResponse
import json, requests


# Create your views here.
def index(request):
    return render(request, 'index.html')


# Nembak json ke /jeson/
def jeson(request):
    json_from_api = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
    dik_tembak = {"data": (json_from_api.json())["items"]}
    return HttpResponse(json.dumps(dik_tembak), content_type="application/json")
