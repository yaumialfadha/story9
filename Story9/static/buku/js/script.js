$(document).ready(function () {
    $('#tabel_buks').DataTable({
        "ajax": {
            "url": '/jeson/',
        },

        "columns": [
            {data: "volumeInfo.title",}, {data: "volumeInfo.authors[,]",},
            {
                orderable: false,
                data: "accessInfo.webReaderLink",
                render: function (data, type, row, meta) {
                    return "<a style='color: cornflowerblue' href='" + data + "'>Link</a>"
                }

            },


            {
                orderable: false,
                data: null,
                render: function (data, type, row, meta) {
                    return "<span class='fa fa-star'></span>"
                }


            }

        ],

    });


    // Apa yang terjadi jika di klik
    var counter = 0;

    $(document).on('click', '.fa', function () {


        $(".angkaCounter").html(counter);

        if (this.className === "fa fa-star") {
            counter++;
            $(".angkaCounter").html(counter);
            $(this).addClass("checked");
        } else {

            counter--;
            $(".angkaCounter").html(counter);
            $(this).removeClass("checked");
        }

    });


});